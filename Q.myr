use std
use math

use t

use "traits"
use "bigint"
use "Z"

pkg yakmo =
        type Q = struct
                p : std.bigint#
                q : std.bigint#
        ;;
        impl abs_struct Q#
        impl disposable Q#
        impl division_struct Q#
        impl field_struct Q#
        impl gcd_struct Q#
        impl group_struct Q#
        impl real_struct Q#
        impl ring_struct Q#
        impl module_struct Q# -> Z#
        impl std.equatable Q#
        impl t.comparable Q#
        impl t.dupable Q#

        generic Qfrom : (p : @a, q : @a -> std.result(Q#, byte[:])) :: numeric,integral @a
        generic QfromZ : (n : @a -> Q#) :: numeric,integral @a
        generic QfromS : (n : byte[:], d : byte[:] -> std.result(Q#, byte[:]))
        const QfromFlt : (f : flt64 -> std.result(Q#, byte[:]))
        const fltfromQ : (q : Q# -> flt64)

        generic eqQ : (q : Q#, n : @i, d : @i -> bool) :: numeric,integral @i
        const reduceQ : (q : Q# -> void)
;;

const __init__ = {
        var q : Q#
        q = q
        std.fmtinstall(std.typeof(q), qfmt)
}

const qfmt = {sb, ap, opts
        var q : Q# = std.vanext(ap)
        std.sbfmt(sb, "{}", q.p)
        if !std.bigeqi(q.q, 1)
                std.sbputs(sb, "/")
                std.sbfmt(sb, "{}", q.q)
        ;;
}

/*
   TODO: Every invocation of this should be replaced with "std.free".
   Typechecking issues that Ori will fix soon.
 */
const freewrap = { a : Q#
        std.bytefree((a : byte#), sizeof(Q))
}

impl disposable Q# =
        __dispose__ = {a : Q#
                var ap : std.bigint# = (a.p : std.bigint#)
                var aq : std.bigint# = (a.q : std.bigint#)
                std.bigfree(ap)
                std.bigfree(aq)
                /* std.free(a) */
                freewrap(a)
        }
;;

impl group_struct Q# =
        gadd_ip = {a : Q#, b : Q#
                /* Be careful in case a == b */
                var t : std.bigint# = std.bigdup(b.p)
                std.bigmul(t, a.q)

                std.bigmul(a.p, b.q)
                std.bigadd(a.p, t)
                std.bigfree(t)

                std.bigmul(a.q, b.q)
                reduceQ(a)
        }
        gadd = {a : Q#, b : Q#
                var aa = t.dup(a)
                gadd_ip(aa, b)
                -> aa
        }
        gneg_ip = {a : Q#
                a.p.sign *= -1
        }
        gneg = {a : Q#
                var aa = t.dup(a)
                aa.p.sign *= -1
                -> aa
        }
        gid = {
                var p : std.bigint# = std.mkbigint(0)
                var q : std.bigint# = std.mkbigint(1)
                var ret : Q = [ .p = p, .q = q]
                -> (std.mk(ret) : Q#) // TODO: Cast shouldn't be necessary.
        }

        eq_gid = {r
                -> std.bigiszero(r.p)
        }
;;

impl ring_struct Q# =
        rid = {
                var p : std.bigint# = std.mkbigint(1)
                var q : std.bigint# = std.mkbigint(1)
                var ret  : Q = [ .p = p, .q = q]
                -> (std.mk(ret) : Q#) // TODO: Cast shouldn't be necessary
        }
        rmul_ip = {a, b
                std.bigmul(a.p, b.p)
                std.bigmul(a.q, b.q)
                reduceQ(a)
        }
        rmul = {a, b
                var q = t.dup(a)
                rmul_ip(q, b)
                -> q
        }
;;

impl division_struct Q# =
        div_maybe = {a, b
                match finv(b)
                | `std.Some bi: -> `std.Some rmul(a, bi)
                | `std.None: -> `std.None
                ;;
        }
;;

impl field_struct Q# =
        finv = {a
                if std.bigiszero(a.p)
                        -> `std.None
                ;;
                var q : Q = [ .p = std.bigdup(a.q), .q = std.bigdup(a.p) ]
                -> `std.Some (std.mk(q) : Q#) // TODO: Cast shouldn't be necessary
        }
;;

impl abs_struct Q# =
        abs_ip = {q; abs_ip(q.p)}
        abs = {q;
                var qq = t.dup(q)
                abs_ip(qq)
                -> qq
        }
        cmp_zero = {q;
                var b : std.bigint# = (q.p : std.bigint#)
                if std.bigiszero(b)
                        -> `std.Equal
                elif b.sign > 0
                        -> `std.After
                else
                        -> `std.Before
                ;;
        }
;;

impl gcd_struct Q# =
        gcd = {a : Q#, b : Q#
                /*
                   Let Thomae's function f(p/q) = 1/q, for p and q
                   coprime. Then gcd(a,b) = a f(b/a) when a != 0, and b
                   if a = 0.
                 */
                var ret
                match finv(a)
                | `std.None:
                        ret = t.dup(b)
                        abs_ip(ret)
                        -> ret
                | `std.Some x:
                        rmul_ip(x, b)
                        var old_p = x.p
                        x.p = std.mkbigint(1)
                        std.bigfree(old_p)
                        rmul_ip(x, a)
                        ret = x
                ;;

                abs_ip(ret)
                -> ret
        }
;;

impl real_struct Q# =
        compconj_ip = {q;}
        compconj = {q; -> t.dup(q)}
;;

impl module_struct Q# -> Z# =
        phi_ip = {z : Z#, q : Q#
                q.p = std.bigmul(q.p, (z : std.bigint#))
                reduceQ(q)
        }
        phi = {z, q
                var qq = t.dup(q)
                phi_ip(z, qq)
                -> qq
        }
;;

impl std.equatable Q# =
        eq = {a, b
                var m = std.bigdup(a.p)
                var n = std.bigdup(b.p)
                std.bigmul(m, b.q)
                std.bigmul(n, a.q)
                var eq = std.bigeq(m, n)
                std.bigfree(m)
                std.bigfree(n)
                -> eq
        }
;;

impl t.comparable Q# =
        cmp = {a,b
                reduceQ(a)
                reduceQ(b)
                var aa = t.dup(a)
                var bb = t.dup(b)
                var m = aa.p
                var n = bb.p
                std.bigmul(m, bb.q)
                std.bigmul(n, aa.q)
                auto aa
                auto bb
                -> std.bigcmp(m, n)
        }
;;

impl t.dupable Q# =
        dup = {a : Q#
                var q : Q = [ .p = std.bigdup(a.p), .q = std.bigdup(a.q) ]
                -> (std.mk(q) : Q#) // TODO: Cast shouldn't be necessary
        }
;;

const reduceQ = {q
        if q.q.sign == -1
                q.q.sign = 1
                q.p.sign *= -1
        ;;

        if std.bigiszero(q.p)
                if !std.bigeqi(q.q, 1)
                        std.bigsteal(q.q, std.mkbigint(1))
                ;;

                -> void
        ;;

        var d : Z# = auto gcd((q.p : Z#), (q.q : Z#))
        std.bigdiv(q.p, (d : std.bigint#))
        std.bigdiv(q.q, (d : std.bigint#))
}

generic eqQ = {q, n, d
        if d == 0
                -> false
        ;;

        var l : std.bigint# = std.bigdup(q.p)
        l = std.bigmuli(l, d)
        var r : std.bigint# = std.bigdup(q.q)
        r = std.bigmuli(r, n)

        var ret = std.bigeq(l, r)
        std.bigfree(l)
        std.bigfree(r)

        -> ret
}

generic Qfrom = {p,q
        var pp : std.bigint# = std.mkbigint(p)
        var qq : std.bigint# = std.mkbigint(q)
        if std.bigiszero(qq)
                std.bigfree(pp)
                std.bigfree(qq)
                -> `std.Err(std.fmt("Denominator is zero"))
        ;;
        var a = std.mk([ .p = pp, .q = qq ])
        reduceQ(a)
        -> `std.Ok a
}

generic QfromZ = {p
        var pp : std.bigint# = std.mkbigint(p)
        var qq : std.bigint# = std.mkbigint(1)
        var a = std.mk([ .p = pp, .q = qq ])
        reduceQ(a)
        -> a
}

generic QfromS = {ps, qs
        match std.bigparse(ps)
        | `std.Some p:
                match std.bigparse(qs)
                | `std.Some q:
                        if std.bigiszero(q)
                                -> `std.Err std.fmt("Denominator is zero")
                        ;;
                        var qbase : Q = [ .p = p, .q = q ]
                        -> `std.Ok std.mk(qbase)
                | `std.None: -> `std.Err std.fmt("Unparsable denominator")
                ;;
        | `std.None: -> `std.Err std.fmt("Unparsable numerator")
        ;;
}

const QfromFlt = {f
        var b = std.flt64bits(f)
        var e, s
        if (b >> 52) & 0x7fful == 0x7fful
                -> `std.Err std.fmt("Cannot convert infinity or NaN to rational")
        ;;

        var isneg : bool = (b >> 63 & 0x1) == 0x1
        if isneg
                b = b & 0x7fffffffffffffff
                f = std.flt64frombits(b)
        ;;

        if b >= 0x4340000000000000
                /*
                   This is ~2^53, certainly an integer. If we go much
                   higher, we can't store the rounding in an int64, so
                   this should be handled specially. We don't need any
                   continued fractions, since this is certainly an
                   integer. Since f = s * 2^(e - 52), build it up.
                 */
                (_, e, s) = std.flt64explode(f)
                var n : std.bigint# = std.mkbigint(s)
                var p : std.bigint# = bigpowtwoi(e - 52)
                std.bigmul(n, p)
                std.bigfree(p)

                if isneg
                        n.sign = -1
                ;;

                var ret : Q = [ .p = n, .q = std.mkbigint(1) ]
                -> `std.Ok (std.mk(ret) : Q#)
        ;;

        /*
           It will be useful to assume later that 0 < a < 1. We can save
           this off safely because of the check for large numbers above
         */
        var a0f : flt64 = math.floor(f)
        var a0 : int64 = math.rn(a0f)
        f = f - a0f
        (_, e, s) = std.flt64explode(f)

        if f == 0.0
                var ret : Q = [ .p = std.mkbigint(a0), .q = std.mkbigint(1) ]
                if isneg
                        ret.p.sign = -1
                ;;
                -> `std.Ok (std.mk(ret) : Q#)
        ;;

        /*
           Now f = s * 2^(e - 52) for 0 < s < 2 and e < 0. We think of f
           as really representing the range (m, M), where

               f- = floating point number immediately below f
               f+ = floating point number immediately above f
               m = (f + f-) / 2
               M = (f + f+) / 2.
        
           We will construct a "best rational approximation" for f as
           follows: begin to compute, in order, the continued fractions
           [ a0; a1, a2, ... ] for each of m (coefficients ai) and M
           (coefficients Ai). One of the following will happen first.

            - If we obtain a k for which ak != Ak, then the best
              rational approximation for f has continued fraction

                [ a0; a1, a2, ... a{k-1}, min(ak, Ak) + 1 ]

              We augment this slightly by, when building up the
              continued fraction, ignoring entries when we can prove
              that the relative error is below 2^-53. In this case, 

            - If we obtain a k for which ak or Ak are ambiguous (this
              will happen at some point, since m and M are rational
              numbers), we give up and return the exact value of f,
              which is s/2^(52 - e) or something similar.

           It is known that, were we to resolve the ambiguity in both
           directions for both m and M, one of the four combinations of
           continued fraction pairs would yield the best rational
           approximation of f. However, that is tedious to calculate and
           doesn't feel like the "right" algorithm.
         */

        /*
           First, get f-, f+, m, and M. Since we've restricted the range
           of f, we can just add and subtract bits to f to get f- and
           f+. Irritatingly, we then have to go to bigints to get the
           numerators and denominators for m and M.
         */
        var fQ : Q# = naiveQfromFlt(f)
        var ret : Q# = fQ
        var fminus : Q# = auto naiveQfromFlt(std.flt64frombits(std.flt64bits(f) - 1))
        var fplus : Q# = auto naiveQfromFlt(std.flt64frombits(std.flt64bits(f) + 1))
        var half : Q# = auto std.mk([ .p = std.mkbigint(1), .q = std.mkbigint(2) ])
        var m : Q# = auto yakmo.gadd(fminus, fQ)
        var M : Q# = auto yakmo.gadd(fplus, fQ)
        yakmo.rmul_ip(m, half)
        yakmo.rmul_ip(M, half)

        /* Now, start computing the continued fractions for m and M */
        var a = [][:]
        var A = [][:]
        while true
                /*
                   Compute ak.

                     m = p/q = 1/(a + epsilon) with epsilon in [0, 1]
                         q/p = a + epsilon

                   We let a = floor(q / p) and epsilon = (q % p) / p.
                   Then setting m = epsilon allows computing the next
                   term.

                   If q % p == 0, then the choice is ambiguous: either
                   (a, 0) or (a - 1, 1) would work. There should be a
                   way to figure out what the correct choice is, but I
                   really don't have time right now, so we bail and
                   return fQ.
                 */
                var ak, mpnext, Ak, Mpnext
                (ak, mpnext) = std.bigdivmod(m.q, m.p)
                if std.bigiszero(mpnext)
                        std.bigfree(ak)
                        std.bigfree(mpnext)
                        goto naive
                ;;
                std.slpush(&a, ak)
                std.bigfree(m.q)
                m.q = m.p
                m.p = mpnext

                (Ak, Mpnext) = std.bigdivmod(M.q, M.p)
                if std.bigiszero(Mpnext)
                        std.bigfree(Ak)
                        std.bigfree(Mpnext)
                        goto naive
                ;;
                std.slpush(&A, Ak)
                std.bigfree(M.q)
                M.q = M.p
                M.p = Mpnext
                match std.bigcmp(ak, Ak)
                | `std.Equal:
                | `std.Before:
                        std.bigaddi(a[a.len - 1], 1)
                        ret = buildCFrac(a0, a)
                        goto done
                | `std.After:
                        std.bigaddi(A[A.len - 1], 1)
                        ret = buildCFrac(a0, A)
                        goto done
                ;;
        ;;

:naive
        var ipart = std.mkbigint(a0)
        std.bigmul(ipart, fQ.q)
        std.bigadd(fQ.p, ipart)
        reduceQ(fQ)

:done
        if (ret != fQ)
                __dispose__(fQ)
        ;;

        for var k = 0; k < a.len; ++k
                std.bigfree(a[k])
        ;;
        std.slfree(a)

        for var k = 0; k < A.len; ++k
                std.bigfree(A[k])
        ;;
        std.slfree(A)

        if isneg
                ret.p.sign = -1
        ;;

        -> `std.Ok ret
}

const naiveQfromFlt = {f
        /* We assume 0 < f < 1 */
        var e, s
        (_, e, s) = std.flt64explode(f)

        var ret : Q = [ .p = std.mkbigint(s), .q = bigpowtwoi(52 - e) ]
        var retp : Q# = std.mk(ret)

        -> retp
}

const bigpowtwoi = {e
        var ret : std.bigint# = std.mkbigint(1)
        std.bigshli(ret, e)
        -> ret
}

const buildCFrac = {a0 : int64, a : std.bigint#[:]
        var q     : Q# = std.mk([ .p = std.mkbigint(1), .q = std.mkbigint(1) ])
        var old_q : Q# = std.mk([ .p = std.mkbigint(2), .q = std.mkbigint(1) ])

        /*
           First, figure out the maximum error that a floating point
           number could detect. If a0 ~ 2^e is not 0, then we can ignore
           errors up to 2^(e - 53).

           But if a0 == 0, we guess based on a1. The number we're
           approximating is not less than 1/(a1 + 1), so we can pull the
           exponent off that, then subtract 53.
         */
        var detectable_exp
        if a0 != 0
                var a0f = (a0 : flt64)
                var n, e, s
                (n, e, s) = std.flt64explode(a0f)
                detectable_exp = 52 - e
        else
                /*
                   a1 is a bigint, so we have to be sloppy. If
                   a1.dig.len > 1, pretend a1 = 2^32. If a1.dig.len ==
                   1, use a1 = a1.dig[0]
                 */
                var a1if
                if a[0].dig.len == 1
                        a1if = 1.0 / (a[0].dig[0] : flt64)
                else
                        a1if = 0.00000000023283064365386962890625
                ;;

                var n, e, s
                (n, e, s) = std.flt64explode(a1if)
                detectable_exp = 52 - e
        ;;
        if detectable_exp < 0
                detectable_exp = 0
        ;;

        var two = std.mkbigint(2)
        std.bigshli(two, detectable_exp)

        for var j = 1; j <= a.len; ++j
                /* Try and build the continued fraction using the first j terms */
                __dispose__(old_q)
                old_q = q

                var ares = a[:j]
                var qbody : Q = [ .p = std.mkbigint(1), .q = std.bigdup(ares[ares.len - 1]) ]
                q = std.mk(qbody)

                for var k = ares.len - 2; k >= 0; --k
                        /* convert p/q to 1/( ares[k] + p/q ) = q / (ares[k]*q + p) */
                        var newq = std.bigdup(ares[k])
                        std.bigmul(newq, q.q)
                        std.bigadd(newq, q.p)
                        std.bigfree(q.p)
                        q.p = std.bigdup(q.q)
                        q.q = newq
                ;;

                /*
                   Was that a detectable improvement? If |q - old_q| <
                   1/2^detectable_exp, then we should return old_q.
                 */
                var qdiff = auto gneg(q)
                gadd_ip(qdiff, old_q)
                std.bigmul(qdiff.p, two)
                match std.bigcmpabs(qdiff.p, qdiff.q)
                | `std.Before:
                        /* std.swap(&q, &old_q) */
                        var t = q
                        q = old_q
                        old_q = t
                        break
                | _:
                ;;
        ;;

        std.bigfree(old_q.p)
        std.bigfree(old_q.q)

        var ipart : std.bigint# = std.bigdup(q.q)
        std.bigmuli(ipart, a0)
        std.bigadd(q.p, ipart)
        std.bigfree(ipart)
        std.bigfree(two)

        reduceQ(q)

        -> q
}

const fltfromQ = {r
        var p : std.bigint# = std.bigdup(r.p)
        var is_neg : bool = false
        if std.bigiszero(p)
                std.bigfree(p)
                -> 0.0
        elif p.sign < 0
                is_neg = true
                p.sign = 1
        ;;

        var q : std.bigint# = std.bigdup(r.q)

        /*
           We want to express

             r = p/q = (1 + (s + eps)/2^52) * 2^e

           where eps in [0, 1), s in [0, 2^52), and e an integer. This
           will allow us to construct the significand out of s + eps and
           u5e e as the exponent. First, calculate e. Luckily, we can
           calculate this in terms of the shifting offset to make p and
           q have the same number of digits.

           There are much fancier ways to get the highest bit of a
           32-bit number, I know.
         */
        var digits_p = 32 * (p.dig.len - 1)
        for var t = p.dig[p.dig.len - 1]; t != 0; t >>= 1
                digits_p++
        ;;
        var digits_q = 32 * (q.dig.len - 1)
        for var t = q.dig[q.dig.len - 1]; t != 0; t >>= 1
                digits_q++
        ;;

        /* Get p and q to have the same order of magnitude */
        var e : int64 = digits_p - digits_q
        if e > 0
                std.bigshli(q, e)
        else
                std.bigshli(p, -e)
        ;;

        /* Now, are we looking at something like 1.01 / 1.02, or 1.02 / 1.01? */
        while true
                /* I'm pretty sure this only takes one pass. */
                match std.bigcmp(p, q)
                | `std.Before:
                        e--
                        std.bigshli(p, 1)
                | _: break
                ;;
        ;;

        /*
           Now that we know e, we may construct

             s + eps = ((r * 2^-e) - 1) * 2^52
                     = p'/q'
                     = floor(p'/q') + eps

           for e' = (p' % q') / q', so floor(p'/q') in [0, 2^52), and eps'
           in [0, 1). We can then round based on eps'.
        */
        std.bigsub(p, q)
        std.bigshli(p, 52)

        var sb, eps_p
        (sb, eps_p) = std.bigdivmod(p, q)
        var s_m_1 = 0
        if sb.dig.len > 0
                s_m_1 += (sb.dig[0] : uint64)
                if sb.dig.len > 1
                        s_m_1 += ((sb.dig[1] : uint64) << 32)
                ;;
        ;;

        /*
           Now, we have to check whether eps_p / q is large enough to
           deserve rounding s up or not. To do so, compare eps_p*2 to q.
         */
        var was_roundup = false
        std.bigshli(eps_p, 1)
        match std.bigcmp(eps_p, q)
        | `std.Before:
        | `std.After:
                s_m_1++
                was_roundup = true
        | `std.Equal:
                if s_m_1 & 0x1 == 0x1
                        s_m_1++
                        was_roundup = true
                ;;
        ;;

        /*
           If we increased s so that it is now 2^52, then
             (1 + s/2^52) * 2^e = (1 + 0) * 2^(e + 1)
         */
        if s_m_1 == 4503599627370496
                s_m_1 = 0
                e++
        ;;
        var s : uint64 = s_m_1 | (1 << 52)

        /* If e is large enough, clean out all the NaN bits to give Inf */
        if e > 1023
                e = 1024
                s = 0
        ;;

        /*
           Finally, we have to handle subnormals. If e is low enough, we
           should truncate s. However, this may require us to do another
           rounding check, and this time we have to compensate for a
           possible previous rounding.
         */
        if e < -1075
                /* This must round to 0 */
                s = 0
                e = -1023
        elif e < -1022
                /*
                   Our s is

                          bit 52     bit 0
                          /          /
                     s = 1xxxxx...xxx
                          \          \
                         2^e        2^e-52

                   The significand of a subnormal number is

                          bit 52     bit 0
                          /          /
                     s = 0xxxxx...xxx
                          \          \
                         2^-1022     2^-1074
                        
                   We'll shift s to the right by e - (-1022), then set e to -1023 
                 */
                var rshift = ((-1022 - e) : uint64)
                var new_s = s >> rshift
                e = -1023
                var firstcut = s & (1 << (rshift - 1))
                var restcut = s & ((1 << (rshift - 1)) - 1)
                var lastkept = s & (1 << (rshift))
                var roundup = firstcut != 0 && (lastkept != 0 || restcut != 0)
                if roundup && !was_roundup
                        new_s++
                        if new_s & (1 << 52) != 0
                                e++   
                        ;;
                ;;
                s = new_s
        ;;

        std.bigfree(p)
        std.bigfree(q)
        std.bigfree(sb)
        std.bigfree(eps_p)
        -> std.flt64assem(is_neg, e, s)
}
